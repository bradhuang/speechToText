package com.example.demo.xunfeiClient;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * @author : YZD
 * @date : 2021-7-20 15:49
 */
public class MyTest {

    public static void main(String[] arg0) throws URISyntaxException {
        // 此处的WebSocket服务端URI，上面服务端第2点有详细说明
        MyWebSocketClient myClient = new MyWebSocketClient(new URI("sdf"));
        // 往websocket服务端发送数据
        myClient.send("此为要发送的数据内容");
    }
}


