package com.example.demo.Interceptor;


import lombok.Getter;

/**
 * 异常的信息通过枚举统一定义，方便定义管理
 *
 * @author : YZD
 * @date : 2021-7-28 16:22
 */
@Getter
public enum ResponseEnum {

    /**
     * 成功
     */
    SUCCESS(200, "成功"),

    /**
     * 参数不正确
     */
    PARAM_ERROR(400, "参数不正确"),

    /**
     * 服务器错误
     */
    SERVER_ERROR(500, "服务器错误"),

    /**
     * 商品不存在
     */
    PRODUCT_NOT_EXIST(10, "商品不存在"),

    ;

    private Integer status;
    private String message;

    ResponseEnum(Integer status, String message) {
        this.status = status;
        this.message = message;
    }

}
