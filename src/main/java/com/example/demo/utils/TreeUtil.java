package com.example.demo.utils;


import com.example.demo.model.SysDepartment;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author yzd
 * @Description: 树形工具类
 * @create 2021-04-27 9:04
 **/
@Service
public class TreeUtil {

    /**
     * 单位树构建 减小复杂度
     *
     * @param childList 组织信息
     * @param orgRoot   根节点
     */
    public static void makeTree(List<SysDepartment> childList, SysDepartment orgRoot) {
        //子类
        List<SysDepartment> children = childList.stream()
                .filter(x -> (!ObjectUtils.isEmpty(x.getParentId())
                        && x.getParentId().equals(orgRoot.getId()))
                ).collect(Collectors.toList());

        if (!CollectionUtils.isEmpty(children)) {
            orgRoot.setChildren(children);
        }

        //后辈中的非子类
        List<SysDepartment> successor = childList
                .stream().filter(x -> !ObjectUtils.isEmpty(x.getParentId())
                        && !x.getParentId().equals(orgRoot.getId()))
                .collect(Collectors.toList());

        if (!CollectionUtils.isEmpty(successor)) {
            children.forEach(x ->
                    makeTree(childList, x)
            );
        }
    }

    /**
     * 求子集
     *
     * @param allOrgList 所有的组织信息
     * @param childList  孩子信息 即 返回体
     * @param rootId     父节点唯一标识
     */
    public static void makeChild(List<SysDepartment> allOrgList, List<SysDepartment> childList, Long rootId) {
        //子类
        List<SysDepartment> children = allOrgList.stream()
                .filter(x -> (!ObjectUtils.isEmpty(x.getParentId())
                        && rootId.equals(x.getParentId()))
                ).collect(Collectors.toList());

        if (!CollectionUtils.isEmpty(children)) {
            childList.addAll(children);
        }

        //后辈中的非子类 减少复杂度
        List<SysDepartment> successor = allOrgList
                .stream().filter(x -> !ObjectUtils.isEmpty(x.getParentId())
                        && !rootId.equals(x.getParentId()))
                .collect(Collectors.toList());

        if (!CollectionUtils.isEmpty(successor) && !CollectionUtils.isEmpty(children)) {

            List<Long> childIdList = children.stream()
                    .map(SysDepartment::getId)
                    .distinct()
                    .collect(Collectors.toList());

            for (Long childId : childIdList) {
                makeChild(successor, childList, childId);
            }
        }
    }

    /**
     * 单位树处理  循环处理 替换递归
     *
     * @param treeList 所有组织信息
     * @param orgRoot  根节点
     * @return
     */
    private List<SysDepartment> childList(List<SysDepartment> treeList, SysDepartment orgRoot) {

        for (SysDepartment outer : treeList) {
            Long outerId = outer.getId();
            List<SysDepartment> childList = new ArrayList<>(treeList.size());
            for (SysDepartment inner : treeList) {
                Long parentId = inner.getParentId();
                if (!ObjectUtils.isEmpty(parentId) && parentId.equals(outerId)) {
                    childList.add(inner);
                }
            }
            outer.setChildren(childList);
        }
        // 根节点
        return treeList.stream()
                .filter(item -> item.getId().equals(orgRoot.getId()))
                .collect(Collectors.toList());
    }


}
